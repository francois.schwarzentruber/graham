#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 13:42:10 2018

@author: fschwarz
"""


""" input: array T, a function key that computes the key of an element
    effect: T is sorted """
def quicksort(T, key):
   _quicksort(T, key, 0,len(T)-1)
   return T


""" input: array T,  a function key that computes the key of an element, first, last two integers
    effect: T[first:last] is sorted """
def _quicksort(T,key, first,last):
   if first<last:
       splitpoint = partition(T,key, first,last)
       _quicksort(T,key,first,splitpoint-1)
       _quicksort(T,key,splitpoint+1,last)

"""input: array T, a function key that computes the key of an element
   effect: in  T[first:last] is decomposed in T[first, i] and T[i+1, last]. All
   elements in  T[first, i]  are <= that the pivot (T[first]) and
   elements in T[i+1, last] are greater. Comparaison are done w.r.t. to function key."""
def partition(T,key, first,last):
   pivotvalue = T[first]

   leftmark = first+1
   rightmark = last

   done = False
   while not done:

       while leftmark <= rightmark and key(T[leftmark]) <= key(pivotvalue):
           leftmark = leftmark + 1

       while key(T[rightmark]) >= key(pivotvalue) and rightmark >= leftmark:
           rightmark = rightmark -1

       if rightmark < leftmark:
           done = True
       else:
           T[leftmark], T[rightmark] = T[rightmark], T[leftmark]

   T[first], T[rightmark] = T[rightmark], T[first] 
   

   return rightmark
