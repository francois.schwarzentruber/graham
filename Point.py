#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 15:54:45 2018

@author: fschwarz
"""

class Point:
    """A point class for use in the Graham Scan."""

    """initialization from coordinates x and y"""
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __str__(self):
        return "(" + self.x + ", " + self.y + ")";