""" implementation of a stack with an array"""
class Stack:
    
    """initialization of the stack. It can contain at most maxCapacity elements"""
    def __init__(self, maxCapacity):
        self._size =	 0
        self._tab = [None] * maxCapacity

    """push an element"""
    def push(self, element):
        if self._size < len(self._tab):
            self._tab[self._size] = element
            self._size = self._size + 1
        else:
            raise ValueError
            
    """pop an element"""
    def pop(self):
        if self._size > 0:
            self._size = self._size - 1
        else:
            raise ValueError
        
    """get the value at the top of the stack"""
    def getTop(self):
        if self._size > 0:
               return self._tab[self._size-1]
        else:
            raise ValueError
        
    """get the value below the top of the stack"""
    def getSecondTop(self):
        if self._size > 1:
               return self._tab[self._size-2]
        else:
            raise ValueError 

    """returns the number of elements in the stack"""
    def getSize(self):
        return self._size
    
    """returns an array of the elements of the stack"""
    def getContent(self):
        return self._tab[0:self._size]