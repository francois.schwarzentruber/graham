#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 16:06:42 2018

@author: fschwarz
"""

import drawSvg as draw
import svgutils.transform as sg



DRAWING_SIZE = 200


"""this class enables to draw pictures for the Graham scan algorithm"""
class DrawingGraham:
    
    """initialization. The number of the current picture is 0"""
    def __init__(self):
        self.number = 0;
        
        
    """start a new drawing of a configuration of the algo"""
    def newDrawing(self):
        self.number = self.number + 1;
        self.d = draw.Drawing(DRAWING_SIZE, DRAWING_SIZE, origin='center')
        self.d.draw(draw.Rectangle(-DRAWING_SIZE/2,-DRAWING_SIZE/2,DRAWING_SIZE,DRAWING_SIZE, fill='#eee'))
                                   
                                   
    """draw the state of stack. closed is true if we want the polygon to be closed"""
    def drawStack(self, stack, closed=False):
        
        p = draw.Path(stroke_width=2, stroke='green',
                      fill='none', fill_opacity=0.5)
        
        stackContent = stack.getContent()
        
        if len(stackContent)>0:
            p.M(stackContent[0].x, stackContent[0].y)
        for point in stackContent:
            p.L(point.x, point.y)  # Draw line to (60, 30)
        
        if closed:
            p.Z()
        self.d.append(p)
        
    """draws a given point with a given color"""
    def drawPoint(self, point, color):
        self.d.append(draw.Circle(point.x, point.y, 4, fill=color, stroke_width=1, stroke=color))
        
    """draw the "OnTheRight" configuration"""
    def drawOnTheRight(self, pointi, second, top):
        p = draw.Path(stroke_width=2, stroke='springgreen',
                      fill='none', fill_opacity=0.5)
        p.M(second.x, second.y)
        p.L(pointi.x, pointi.y)
        self.d.append(p)
        self.drawPoint(top, "red")
        
    """draw a configuration of the algo"""
    def draw(self, points, stack, pivot, i):
        FONT_SIZE = 14
        TEXT_SHIFT = 10
        j = 0
                
        self.drawStack(stack)
        for point in points:
            j = j + 1
            self.d.append(draw.Circle(point.x, point.y, 2, fill='black', stroke_width=1, stroke='black'))
            self.d.append(draw.Text(str(j),FONT_SIZE,point.x, point.y-TEXT_SHIFT, center=0.6, fill='black'))
        
        if pivot != None:
            self.drawPoint(pivot, 'blue')
            
        for point in stack.getContent():
            self.drawPoint(point, 'green')
            
        
            
        if i != None:
            self.drawPoint(points[i], 'black')

    """save the current picture in the corresponding file"""
    def save(self):
        self.d.saveSvg('draw/draw' + str(self.number) + '.svg')
    
    """get name for a subfig in the big picture"""
    def getNameSubFig(self, j):
        return "Etape " + str(j-3)
    
    """once the pictures have been saved, it produces a big svg file with all the configurations displayed"""
    def createBigPicture(self):
        #create new SVG figure
        fig = sg.SVGFigure("400cm", "220cm")
        
        XMIN = 100
        x = XMIN
        y = 100
        for j in range(4,self.number+1):
            subfig = sg.fromfile('draw/draw' + str(j) + '.svg')
            root = subfig.getroot()
            root.moveto(x, y)
            txt = sg.TextElement(x-90,y-85, self.getNameSubFig(j), size=16, weight="bold")
            fig.append(root)
            fig.append(txt)
            if x > 900:
                x = XMIN
                y = y + DRAWING_SIZE + 10
            else:
                x = x + DRAWING_SIZE + 10
            print(j)
        fig.save("draw/fig_final.svg")