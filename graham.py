import math
from quicksort import *
from Stack import Stack
from Point import Point
from DrawingGraham import DrawingGraham
import random

""" returns a random set of points"""
def getRandomPoints():
    points = [];
    NB_POINTS = 12
    COORD_RANGE = 180
    for i in range(1, NB_POINTS):
        points.append(Point(random.randint(-COORD_RANGE, COORD_RANGE), 
                            random.randint(-COORD_RANGE, COORD_RANGE)));
    return points;
    

""" input: a set of points
    output: the pivot (the point with the smallest y, then smallest x)
    effect: the pivot is in points[0]"""
def getPivot(points):
    ipivot = 0
    for i in range(len(points)):
        point = points[i]
        pivot = points[ipivot]
        if(point.y < pivot.y) | ((point.y == pivot.y) & (point.x < pivot.x)):
            ipivot = i
    points[0], points[ipivot] = points[ipivot], points[0]
    return pivot
    

""" input: two points A, B
    output: their angle"""
def getAngle(A, B):
    if A == B:
        return math.pi
    else:
        return math.atan2(B.y - A.y, B.x - A.x)

""" input: three points
    output: yes if C is on the left of AB"""
def isOnTheRight(A, B, C):
    return (B.y - A.y) * (C.x - A.x) - (B.x - A.x) * (C.y - A.y) >= 0
    
""" input: a set of points
    output: the convex hull of the set of points
    effect: it also produces a lot of svg pictures :)"""
def grahamScan(points):
    stack = Stack(len(points))
    D = DrawingGraham()
    D.newDrawing();
    D.draw(points, stack, None, None)
    D.save()
    pivot = getPivot(points)
    
    D.newDrawing();
    D.draw(points, stack, pivot, None)
    D.save()
    
    
    points[1:] = quicksort(points[1:], key = (lambda A : getAngle(pivot,A)))
    
    D.newDrawing();
    D.draw(points, stack, pivot, None)
    D.save()
    
    stack.push(points[0])    
    stack.push(points[1])
        
    D.newDrawing();
    D.draw(points, stack, pivot, None)
    D.save()
    

    for i in range(2, len(points)):
        D.newDrawing();
        D.draw(points, stack, pivot, i)
        D.save()
        
        while (stack.getSize() >= 2) & isOnTheRight(points[i], stack.getSecondTop(), stack.getTop()):
            D.newDrawing();
            D.draw(points, stack, pivot, i)
            D.drawOnTheRight(points[i], stack.getSecondTop(), stack.getTop())
            D.save()
            stack.pop()        

        stack.push(points[i])
        D.newDrawing();
        D.draw(points, stack, pivot, i)
        D.save()

    D.newDrawing();
    D.drawStack(stack, closed=True)
    D.draw(points, stack, pivot, i)
    D.save()
    
    D.createBigPicture()
    
    return stack.getContent()
    


grahamScan([Point(40, 40), 
            Point(72, 15), 
            Point(-10, -20), 
           # Point(0, -170),
            Point(95, -40),
            #Point(-100, 90), 
            #Point(-40, 180),
            Point(0, 0),
            Point(0, 20), 
            Point(-98, -85),
            Point(-50, 50),
            Point(-60, -35)])